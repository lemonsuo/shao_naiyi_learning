docker stop hadoop-master
docker stop hadoop-slave1
docker stop hadoop-slave2

echo stop containers 
docker rm hadoop-master
docker rm hadoop-slave1
docker rm hadoop-slave2

echo rm containers

docker ps
