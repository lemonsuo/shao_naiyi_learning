//定义所需要搜索的字符串
var str = 'wechat:shaonaiyi888,520777';
//方法1、不使用正则表达式
function findNum(str) {
	var tmp = '',
		arr = [];
	for(var i = 0; i < str.length; i++) {
		var current = str[i];
		if(!isNaN(current)) {
			tmp += current;
		} else {
			if(tmp) {
				arr.push(tmp);
				tmp = '';
			}
		}
	}
	if(tmp) {
		arr.push(tmp)
	}
	return arr;
}
console.log("不使用正则表达式：" + findNum(str))
//方法2、使用正则表达式
var reg = /\d+/g;
console.log("使用正则表达式：" + str.match(reg))

var str1 = 'Hi,hi,hi shaonaiyi,Hello';
//g:修饰符，表示全局匹配;\b是一个元字符，代表单词边界，匹配单词的开头和结尾。
//1、实例创建方式：
var reg1 = new RegExp("\\bhi\\b", "g");
//2、字面量创建法:
reg2 = /\bhi\b/gi;
console.log(str1.match(reg1)); //"hi,hi"
console.log(str1.match(reg2)); //"Hi,hi,hi"
//匹配shenzhen，并匹配后面的shaonaiyi
var str2 = /\bshenzhen\b.*\bshaonaiyi\b/;
var welcome = 'I love shenzhen,shaonaiyi!!';
console.log(welcome.match(str2)); //"shenzhen,shaonaiyi"