var arr = [1, 2, 3]
console.log(arr);
arr.push(4, 5);
console.log(arr);
arr.shift()
console.log(arr);
arr.unshift(0);
console.log(arr);

//排序

var users = [{
		name: 'king',
		age: 12
	},
	{
		name: 'shaonaiyi',
		age: 22
	},
	{
		name: 'lucky',
		age: 34
	},
	{
		name: 'tony',
		age: 18
	},
];

users.sort(function(a, b) {
	if(a.name > b.name) return 1;
	if(a.name < b.name) return -1;
	return 0;
});

for (var i in users){
	console.log(users[i]['name']);
}

