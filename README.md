----
#### 0x00 介绍

邵奈一的学习库：Java、Python、Hadoop、Spark、Flink...
技术栈：后端、大数据、人工智能、前端

----
#### 0x01 项目结构
```
bigdata:大数据相关
docker：复制粘贴玩转大数据专栏
mmall_fe：前端商城
study_fe：前端学习
vue-project：vue案例
README.md
```
----
#### 0x02 优质链接
1. 邵奈一CSDN博客： [https://blog.csdn.net/shaock2018](https://blog.gitee.com)
2. 邵奈一的学习库码云链接：[https://gitee.com/shaock/shao_naiyi_learning](https://gitee.com/shaock/shao_naiyi_learning)
3. 邵奈一的技术博客导航链接：[https://blog.csdn.net/shaock2018/article/details/89349249](https://blog.csdn.net/shaock2018/article/details/89349249)
4. 更多优秀项目，请参考码云地址： [https://gitee.com/shaock](https://gitee.com/shaock) 
----

**作者简介：**[邵奈一](https://blog.csdn.net/shaock2018/article/details/91184254)
**全栈工程师、市场洞察者、专栏编辑**
| [公众号](https://img-blog.csdnimg.cn/20190608000417972.jpeg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NoYW9jazIwMTg=,size_16,color_FFFFFF,t_70) | [微信](https://img-blog.csdnimg.cn/20190514084602748.png) | [微博](https://weibo.com/5680168183/) | [CSDN](https://blog.csdn.net/shaock2018) | [简书](https://www.jianshu.com/u/6f033c6a3b90) |

----