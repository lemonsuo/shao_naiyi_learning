package com.shaonaiyi.hadoop.filetype.parquet;

import com.shaonaiyi.hadoop.filetype.avro.Person;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroParquetReader;
import org.apache.parquet.avro.AvroParquetWriter;
import org.apache.parquet.hadoop.ParquetReader;
import org.apache.parquet.hadoop.ParquetWriter;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;

import java.io.IOException;

/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/18 11:11
 * @Description 编写读写Parquet文件Demo
 */
public class AvroParquetDemo {

    public static void main(String[] args) throws IOException {
        Person person = new Person();
        person.setName("shaonaiyi");
        person.setAge(18);
        person.setFavoriteNumber(7);
        person.setFavoriteColor("red");

        Path path = new Path("hdfs://master:9999/user/hadoop-sny/mr/filetype/avro-parquet2");

        ParquetWriter<Object> writer = AvroParquetWriter.builder(path)
                .withSchema(Person.SCHEMA$)
                .withCompressionCodec(CompressionCodecName.SNAPPY)
                .build();

        writer.write(person);

        writer.close();

        ParquetReader<Object> avroParquetReader = AvroParquetReader.builder(path).build();
        Person record = (Person)avroParquetReader.read();
        System.out.println("name：" + record.getName());
        System.out.println("age：" + record.get("age").toString());
        System.out.println("favorite_number：" + record.get("favorite_number").toString());
        System.out.println("favorite_color：" + record.get("favorite_color"));

    }


}
