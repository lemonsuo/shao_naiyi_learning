package com.shaonaiyi.hadoop.filetype.sequence;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import java.io.IOException;
import java.net.URI;

/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/20 11:27
 * @Description Hadoop支持的文件格式之写Sequence
 */
public class SequenceFileWriter {

    private static final String[] DATA = {
            "shao, naiyi, bigdata, hadoop",
            "naiyi, bigdata, spark",
            "yi, two, a good man"
    };
    public static void main(String[] args) throws IOException {
        String uri = "hdfs://master:9999/user/hadoop-sny/mr/filetype/sequence.seq";

        Configuration configuration = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(uri), configuration);
        Path path = new Path(uri);

        IntWritable key = new IntWritable();
        Text value = new Text();
        SequenceFile.Writer writer = null;
        try {
            writer = SequenceFile.createWriter(configuration,
                    SequenceFile.Writer.file(path), SequenceFile.Writer.keyClass(key.getClass()),
                    SequenceFile.Writer.valueClass(value.getClass()));

            for (int i = 0; i < 100; i++) {
                key.set(100 -i);
                value.set(DATA[i % DATA.length]);
                System.out.printf("[%s]\t%s\t%s\n", writer.getLength(), key, value);
                writer.append(key, value);
            }
        } finally {
            writer.close();
        }

    }

}
