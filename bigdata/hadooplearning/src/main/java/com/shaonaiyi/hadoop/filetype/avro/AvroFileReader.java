package com.shaonaiyi.hadoop.filetype.avro;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;

import java.io.File;
import java.io.IOException;
/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/17 16:48
 * @Description 编码实现读Avro文件
 */
public class AvroFileReader {

    public static void main(String[] args) throws IOException {

        File file = new File("person.avro");
        DatumReader<GenericRecord> reader = new GenericDatumReader<>();
        DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file, reader);

        GenericRecord record = null;
        while (dataFileReader.hasNext()) {
            record = dataFileReader.next();
            System.out.println("name:" + record.get("name").toString());
            System.out.println("age:" + record.get("age").toString());
            System.out.println("favorite_number:" + record.get("favorite_number").toString());
            System.out.println("favorite_color:" + record.get("favorite_color"));
            System.out.println("-----------------------------------");
        }

    }

}
