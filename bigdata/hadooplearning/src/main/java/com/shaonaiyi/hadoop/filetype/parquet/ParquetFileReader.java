package com.shaonaiyi.hadoop.filetype.parquet;

import org.apache.hadoop.fs.Path;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.hadoop.ParquetReader;
import org.apache.parquet.hadoop.example.GroupReadSupport;

import java.io.IOException;

/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/18 10:18
 * @Description 编码实现读Parquet文件
 */
public class ParquetFileReader {

    public static void main(String[] args) throws IOException {


        Path path = new Path("hdfs://master:9999/user/hadoop-sny/mr/filetype/parquet/parquet-data.parquet");
        GroupReadSupport readSupport = new GroupReadSupport();
        ParquetReader<Group> reader = new ParquetReader<>(path, readSupport);

        Group result = reader.read();
        System.out.println("name：" + result.getString("name", 0).toString());
        System.out.println("age：" + result.getInteger("age", 0));
        System.out.println("favorite_number：" + result.getInteger("favorite_number", 0));
        System.out.println("favorite_color：" + result.getString("favorite_color", 0));
    }

}
