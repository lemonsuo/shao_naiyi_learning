package com.shaonaiyi.hadoop;

import com.shaonaiyi.hadoop.filetype.avro.Person;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.parquet.avro.AvroParquetInputFormat;
import org.apache.parquet.avro.AvroParquetOutputFormat;
import org.apache.parquet.hadoop.ParquetOutputFormat;
import org.codehaus.jackson.schema.SchemaAware;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @Auther: 邵奈一
 * @Date: 2019/03/21 下午 7:02
 * @Description: WordCount入门例子之单词计数（Java版）
 * 使用脚本：hadoop jar hadoop-learning-1.0.jar com.shaonaiyi.hadoop.WordCount hdfs://master:9999/files/put.txt hdfs://master:9999/output/wc/
 */
public class WordCount {

    public static class MyMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

        LongWritable one = new LongWritable(1);

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String lines = value.toString();
            String[] words = lines.split(" ");
            for (String word: words){
                context.write(new Text(word), one);
            }

        }


    }

    public static class MyReducer extends Reducer<Text, LongWritable, Text, LongWritable> {

        @Override
        protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {

            int sum = 0;
            for (LongWritable value: values){
                sum += value.get();
            }
            TimeUnit.SECONDS.sleep(10);
            context.write(key, new LongWritable(sum));

        }
    }

    public static void main(String[] args) throws Exception{

        Configuration configuration = new Configuration();

        // 若输出路径有内容，则先删除
        Path outputPath = new Path(args[1]);
        FileSystem fileSystem = FileSystem.get(configuration);
        if(fileSystem.exists(outputPath)){
            fileSystem.delete(outputPath, true);
            System.out.println("路径存在，但已被删除");
        }


        Job job = Job.getInstance(configuration, "WordCount");

        job.setJarByClass(WordCount.class);

        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        job.setNumReduceTasks(3);

        job.setInputFormatClass(AvroParquetInputFormat.class);
        AvroParquetInputFormat.setAvroReadSchema(job, Person.SCHEMA$);

        job.setOutputFormatClass(ParquetOutputFormat.class);
        AvroParquetOutputFormat.setSchema(job, Person.SCHEMA$);

//        FileOutputFormat.setCompressOutput(job, true);
//        FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));


        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }

}
