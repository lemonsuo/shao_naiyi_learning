package com.shaonaiyi.hadoop.serialize;

import java.io.Serializable;

/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/13 16:18
 * @Description 定义需要序列化的Block类
 */
public class Block implements Serializable {

    private long blockId;
    private long numBytes;
    private long generationStamp;

    public Block(long blockId, long numBytes, long generationStamp) {
        this.blockId = blockId;
        this.numBytes = numBytes;
        this.generationStamp = generationStamp;
    }

    public Block() {

    }

    public long getBlockId() {
        return blockId;
    }

    public long getNumBytes() {
        return numBytes;
    }

    public long getGenerationStamp() {
        return generationStamp;
    }

    @Override
    public String toString() {
        return "Block{" +
                "blockId=" + blockId +
                ", numBytes=" + numBytes +
                ", generationStamp=" + generationStamp +
                '}';
    }

}
