package com.shaonaiyi.hadoop.serialize;

import java.io.*;

/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/13 16:20
 * @Description 编写序列化与反序列化测试类代码
 */

public class JavaSerializableTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        String fileName = "blockByte.txt";
//        serialize(fileName);
        deSerialize(fileName);

    }

    private static void serialize(String fileName) throws IOException {
        Block block = new Block(78062621L, 39447651L, 56737546L);

        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }

        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(block);
        objectOutputStream.close();

    }

    private static void deSerialize(String fileName) throws IOException, ClassNotFoundException {

        FileInputStream fileInputStream = new FileInputStream(fileName);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Object object = objectInputStream.readObject();
        Block block = (Block)object;
        System.out.println(block);
    }

}

