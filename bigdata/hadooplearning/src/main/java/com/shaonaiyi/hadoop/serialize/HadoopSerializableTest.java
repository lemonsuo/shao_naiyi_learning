package com.shaonaiyi.hadoop.serialize;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableFactories;
import java.io.*;

/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/13 16:53
 * @Description 编写Hadoop序列化机制测试类代码
 */
public class HadoopSerializableTest {

    public static void main(String[] args) throws IOException {
        String fileName = "blockWritable.txt";

//        serialize(fileName);
        deSerialize(fileName);

        Text text = new Text();
        String tem = "hello";
        text.set(tem);

        IntWritable intWritable = new IntWritable(3);
    }

    private static void serialize(String fileName) throws IOException {
        BlockWritable block = new BlockWritable(78062621L, 39447651L, 56737546L);

        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
        block.write(dataOutputStream);
        dataOutputStream.close();
    }

    private static void deSerialize(String fileName) throws IOException {

        FileInputStream fileInputStream = new FileInputStream(fileName);
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        Writable writable = WritableFactories.newInstance(BlockWritable.class);
        writable.readFields(dataInputStream);
        System.out.println((BlockWritable)writable);

    }

}
