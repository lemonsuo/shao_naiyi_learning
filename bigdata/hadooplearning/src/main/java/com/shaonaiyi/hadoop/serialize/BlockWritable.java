package com.shaonaiyi.hadoop.serialize;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @Author shaonaiyi@163.com
 * @Date 2019/12/13 16:51
 * @Description 定义引入Hadoop序列化机制的BlockWritable类
 */
public class BlockWritable implements Writable {

    private long blockId;
    private long numBytes;
    private long generationStamp;

    public BlockWritable() {
    }

    public BlockWritable(long blockId) {
        this.blockId = blockId;
    }

    public BlockWritable(long blockId, long numBytes, long generationStamp) {
        this.blockId = blockId;
        this.numBytes = numBytes;
        this.generationStamp = generationStamp;
    }

    public long getBlockId() {
        return blockId;
    }

    public long getNumBytes() {
        return numBytes;
    }

    public long getGenerationStamp() {
        return generationStamp;
    }

    @Override
    public String toString() {
        return "Block{" +
                "blockId=" + blockId +
                ", numBytes=" + numBytes +
                ", generationStamp=" + generationStamp +
                '}';
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(blockId);
        dataOutput.writeLong(numBytes);
        dataOutput.writeLong(generationStamp);
    }

    public void readFields(DataInput dataInput) throws IOException {
        this.blockId = dataInput.readLong();
        this.numBytes = dataInput.readLong();
        this.generationStamp = dataInput.readLong();
    }

}
